<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/', function () {
    return view('homepage');
});
Route::get('/contact_us', function () {
    return view('contact_us');
});
Route::get('/help', function () {
    return view('help');
});
Route::get('/help1', function () {
    return view('help1');
});
Route::get('/help2', function () {
    return view('help2');
});
Route::get('/help3', function () {
    return view('help3');
});
Route::get('/help4', function () {
    return view('help4');
});
Route::get('/ride', function () {
    return view('ride');
});
Route::get('/dashboard', function () {
    return view('dashboard');
});
Route::get('/dashboardadmin', function () {
    return view('dashboardadmin');
});
Route::post('/ridebook', function () {
    return view('ridebook');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/dashboard', 'AdminController@admin')
    ->middleware('is_admin')
    ->name('admin');
Route::get('/ride', 'RideController@index')
    ->middleware('ride')
    ->name('index');

