@extends('layouts.app')

@section('content')
    <!-- Input form for contact us!-->
    <div id="after_submit">
        <form id="contact_form" action="#" method="POST" enctype="multipart/form-data">
            <div class="row">
                <input id="name" class="input" name="name" placeholder="Your name" type="text" value="" size="30" />
            </div><br>
            <div class="row">
                <input id="email" class="input" name="email" placeholder="youremail@hotmail.com" type="text" value="" size="30" />
            </div><br>
            <div class="row">
                <textarea id="message" class="input" name="message" placeholder ="Type in your message" rows="7" cols="30"></textarea>
            </div><br>
            <input id="submit_button" type="submit" value="Send email" />
        </form></div><br><br>
    <div class="ContactInfo">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.<br>
        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.<br>
        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.<br>
        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    </div>
    @include('format.footer')

@endsection