@extends('layouts.app')

@section('content')
    <div class="container">
        Adding and selecting a preferred payment method will allow you request a ride. <br>
        Depending on your country and city, you can add payment methods such as credit cards, <br>
        cash, or a PayPal account. When a ride ends, your selected payment method is charged.<br>
        <br>

        During a ride, use your app to check that your preferred payment method is selected. <br>
        Swipe up from the bottom of the app and tap your desired payment method before the trip ends.<br>
        <br>

        ADD A PAYMENT METHOD<br>
        1. Select "Payment" from your app menu.<br>
        2. Tap Add Payment.<br>
        3. Add a payment method by scanning a card, manually entering card info, or adding an alternative payment type.<br>
        <br>

        SCAN A CREDIT OR DEBIT CARD<br>
        1. To scan a card, tap the camera icon. Your phone may ask permission for the Hyperion app to use the camera.<br>
        2. Center your card in your phone's screen so that all 4 corners flash green. Cards with embossed letters and <br>
        numbers are typically easiest to scan.<br>
        3. Enter the card's expiration date, CVV number, and billing ZIP or postal code.<br>
        4. Tap SAVE.<br>
        <br>

        MANUALLY ADD A CREDIT OR DEBIT CARD<br>
        1. Enter your card number.<br>
        2. Enter the expiration date, CVV number, and billing ZIP or postal code.<br>
        3. Tap SAVE.<br>

    </div>
    @include('format.footer')

@endsection