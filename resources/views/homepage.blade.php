@extends('layouts.app')

@section('content')
    @include('format.header')
    <div class="container">
        <section id="home-hero">

            <h1>Get There - Your Day Belongs To You</h1>

        </section>
    </div>
    <section id="services">
        <div class="container">


            <div class="serviceMo">
                <img style="display: inline; margin: 0 5px;" title="Simple touch" src="images/single%20touch.jpg" alt="" width="50" height="50"/>
                <h3>Easiest Way Around</h3>
                <p>One tap and the car comes directly to you. Hop in, your driver knows exactly where to go and when you get there just step out. Payment is completly seemless.</p>
            </div>

            <div class="serviceMa">
                <img style="display: inline; margin: 0 5px;" title="Map" src="images/anywhere.jpg" alt="" width="50" height="50"/>
                <h3>Anywhere, Anytime</h3>
                <p>Daily commute. Errand across town. Early morning flight. Late night drinks. Wherever your headed, count on Hyperion Cabs for a ride.</p>
            </div>


            <div class="serviceBa">
                <img style="display: inline; margin: 0 5px;" title="Bank" src="images/piggy-bank-icon.png" alt="" width="50" height="50"/>
                <h3>Low Cost to Luxury</h3>
                <p>Economy cars at everyday prices are always available. For special occasions or when you just need a bit more room, book one of our luxury cars.</p>
            </div>
        </div>
    </section>
    <section id="map-fare">
        <div class="container">
            <div id="form">
                Book your journey!<br>
                <div id="bnow">
                <a href="{{ url('ride') }}">Book Now</a>
                </div>

            </div>
            <div id="map">
                <script>
                    function initMap() {
                        var uluru = {lat: -25.363, lng: 131.044};
                        var map = new google.maps.Map(document.getElementById('map'), {
                            zoom: 4,
                            center: uluru
                        });
                        var marker = new google.maps.Marker({
                            position: uluru,
                            map: map
                        });
                    }
                </script>
                <script async defer
                        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDD339QE8dQAo3jCi_MCrytkckSEYZw1Sw&callback=initMap">
                </script>
            </div>

        </div>
    </section>

    @include('format.footer')

@endsection