@extends('layouts.app')

@section('content')
    <div class="DashTitle">
        <h1>Managers Board</h1>
    </div>
    <div class="container">
        <div class="DashF1">
            <form class ="Details-revenue">
                <br>
                <p>Number of travels last month:
                    <input type="text" name="Number of travel last M" value="90"></p>
                <p>Number of travels this month:
                    <input type="text" name="Number of travel this M" value="105"></p>
                <br><br>
                <p>Last month profit:
                    <input type="text" name="Last M Profit" value="£40000"></p>
                <p>This month profit:
                    <input type="text" name="This M Profit" value="£40500"></p>
                <br><br>
                <p>Most favourite driver this Month:
                    <input type="text" name="Month favourite driver" value="John Kinoa"></p>
                <p>Least favourite driver this Month:
                    <input type="text" name="Month least favourite driver" value="Dolbris argura"></p>
                <br>

            </form>
        </div>

        <div class="DashF2">
            <form class ="Details-revenue">
                <br>
                <p>Current top 5 most favorite driver this year</p>
                <input type="text" name="f1" value="Edgar Montenegro"><br>
                <input type="text" name="f2" value="Plavis Pael"><br>
                <input type="text" name="f3" value="Prililin Diaz"><br>
                <input type="text" name="f4" value="John Kinoa"><br>
                <input type="text" name="f5" value="Mataruano Fonseca"><br>
            </form>
        </div>

        <div class="Members">
            <p>List of registered users</p>
            <textarea rows="30" cols="30"></textarea>
        </div>
    </div>
    @include('format.footer')

@endsection