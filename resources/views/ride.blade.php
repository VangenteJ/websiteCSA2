@extends('layouts.app')

@section('content')
    <section id="estimate-form">
        <div class="container">
            <div id="form">
                <form action="ridebook" method="POST">
                    {{ csrf_field() }}
                    <h3>Pricing</h3>
                    <h3>Get a fare estimate</h3>
                    Start Location:<br>
                    <input type="text" name="startlocation" placeholder="From"><br>
                    End Location:<br>
                    <input type="text" name="endlocation" placeholder="Destination"><br>
                    Date:<br>
                    <input type="date" name="faredate" placeholder="dd/mm/yyyy"><br>
                    <input type="submit" value="Enter">
                </form><br><br>
            </div>

            <div id="map">
                <script>
                    function initMap() {
                        var uluru = {lat: -25.363, lng: 131.044};
                        var map = new google.maps.Map(document.getElementById('map'), {
                            zoom: 4,
                            center: uluru
                        });
                        var marker = new google.maps.Marker({
                            position: uluru,
                            map: map
                        });
                    }
                </script>
                <script async defer
                        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDD339QE8dQAo3jCi_MCrytkckSEYZw1Sw&callback=initMap">
                </script>
            </div>
        </div>
    </section><br><br>
    @include('format.footer')

@endsection