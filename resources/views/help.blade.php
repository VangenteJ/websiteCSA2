@extends('layouts.app')

@section('content')
    <div class="container">
    <section id="help-hero">

        <div id="searchhelp">
            <h3>Having Trouble??<br>We are here to help</h3>
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
            <form action="action_page.php">
                <input type="text" placeholder="Search.." name="search">
                <button type="submit"><i class="fa fa-search"></i></button>
            </form>
        </div>

    </section>
    </div>

    <section id="help">
        <div class="container">


            <div class="help">
                <h3>Account and Payment Options</h3>
                <p>I cannot sign in or request a cab.<br>
                    Changing my account settings.<br>
                    Payment Options.</p>
                <div id="mcolor">
                <a class="more" href="{{ url('help2') }}">More</a>
                </div>
            </div>

            <div class="help">
                <h3>A Guide to Hyperion Cabs</h3>
                <p>Requesting a trip.<br>
                    Taking a ride.<br>
                    After my ride.</p>
                <div id="mcolor">
                <a class="more" href="{{ url('help4') }}">More</a>
                </div>
            </div>


            <div class="help">
                <h3>More</h3>
                <p>Legal, privacy and other issues.<br>
                    Using Hyperion Cabs for delivery.<br>
                    Using Hyperion Cabs at the airport.</p>
                <div id="mcolor">
                <a class="more" href="{{ url('help3') }}">More</a>
                </div>
            </div>

            <div class="help">
                <h3>Accessibility</h3>
                <p>How to use VoiceOver.<br>
                    What is Hyperion Cabs policy about assistive technologies?<br>
                    How to use TalkBack.</p>
                <div id="mcolor">
                <a class="more" href="{{ url('help1') }}">More</a>
                </div>
            </div>
        </div>
    </section>

    @include('format.footer')

@endsection