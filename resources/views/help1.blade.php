@extends('layouts.app')

@section('content')
    <div class="container">
        Requesting a ride<br>
        SETTING YOUR DESTINATION<br>
        On the home screen, double tap on the "Where to?" <br>
        button. This will activate destination search where <br>
        you can search or select from a list of suggested destinations. <br>
        Additionally, Uber may suggest "shortcuts" which appear along the <br>
        bottom of the home screen. Double tap on any of these to select it <br>
        as your destination, then proceed to the confirmation screen.<br>

        CHANGING PICKUP LOCATION<br>
        Pickup location will be set automatically using GPS, but if you <br>
        wish to change it please follow these instructions. On the home <br>
        screen, double tap on the "Where to?" button. Before choosing your destination, <br>
        flick left to highlight the "Pickup location" field and double tap to edit. <br>
        Once you have entered your desired pickup location, the app will <br>
        automatically switch you back over to destination entry mode.<br>

        MENU OPTIONS<br>
        The Menu can be accessed by tapping on the top left of the screen. <br>
        Options available (from top to bottom): Payment, Your trips, Free Rides, Help, and Settings.

        Payment: Allows you to change or add a payment method.<br>
        Your trips: Allows you to view past trips in order to get help <br>
        or view receipts. It also allows you to view upcoming scheduled trips.<br>
        Free rides: Allows you to view your invite code and send invites to your friends.<br>
        Help: Provides a variety of support options including the ability to report accessibility issues.<br>
        Settings: Allows you to update your favorite locations which will be available for easy clicking <br>
        on the home screen so you don't always have to enter your destination manually.<br>

    </div>
    @include('format.footer')

@endsection