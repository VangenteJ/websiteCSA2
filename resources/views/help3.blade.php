@extends('layouts.app')

@section('content')
    <div class="container">
        If you were the rider who requested pickup for a trip that involved a serious incident, <br>
        please head to the link below. This will help us support you immediately. <br>
        If you're viewing this page with a web browser at help.uber.com, make sure you're signed in to your Hyperion account.<br>

        If you wish to report a serious incident involving an Uber rider, driver, or vehicle, <br>
        please let us know here by sharing some info at the bottom of this page. If you were a <br>
        co-rider on a trip requested by a friend or a third-party witness to an incident, please reach out. We are here to help.<br>

        Uber enforces strict safety guidelines to keep rides safe and comfortable. <br>
        Unprofessional driver behavior like inappropriate physical contact or verbal aggression are not tolerated.<br>

    </div>
    @include('format.footer')

@endsection