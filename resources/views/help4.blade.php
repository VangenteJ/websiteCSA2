@extends('layouts.app')

@section('content')
    <div class="container">
        <div id="HGuide">
            With Hyperion cabs, <br>
            you just need to book <br>
            a taxi through our system, <br>
            we will get to your location and<br>
            you will be charged for the distance <br>
            of the travel regardless of the time.
        </div>

    </div>
    @include('format.footer')

@endsection