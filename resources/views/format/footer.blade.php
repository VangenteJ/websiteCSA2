<!-- Footer section!-->
<section id="footerimage">
    <div class="container">
        <a href="homepage"><img style="display: inline; margin: 0 5px;" title="Facebook" src="images/if_logo-facebook_350974.png" alt="" width="50" height="50"/></a>
        <a href="homepage"><img style="display: inline; margin: 0 5px;" title="Instagram" src="images/if_logo-instagram_350985.png" alt="" width="50" height="50"/></a>
        <a href="homepage"><img style="display: inline; margin: 0 5px;" title="Twitter" src="images/if_logo-twitter_351003.png" alt="" width="50" height="50"/></a>

    </div>
</section>

<footer>
    <div class="container">
        <div id="copyright">
            <p>Hyperion Cabs, Copyright &copy; 2017</p>
        </div>
        <nav>
            <ul>
                <li><a href="/">Home</a></li>
                <li><a href="ride">Ride</a></li>
                <li><a href="dashboard">Dashboard</a></li>
                <li><a href="help">Help</a></li>
                <li><a href="contact_us">Contact</a></li>
            </ul>
        </nav>
    </div>
</footer>
</body>
</html>