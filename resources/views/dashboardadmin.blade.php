@extends('layouts.app')

@section('content')
    <div class="container">
        You need to be an admin to view this page.
    </div>
    @include('format.footer')

@endsection